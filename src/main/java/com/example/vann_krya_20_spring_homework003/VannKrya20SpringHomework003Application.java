package com.example.vann_krya_20_spring_homework003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VannKrya20SpringHomework003Application {

    public static void main(String[] args) {
        SpringApplication.run(VannKrya20SpringHomework003Application.class, args);
    }

}
