package com.example.vann_krya_20_spring_homework003.controller;


import com.example.vann_krya_20_spring_homework003.model.entity.Author;
import com.example.vann_krya_20_spring_homework003.model.request.AuthorRequest;
import com.example.vann_krya_20_spring_homework003.model.response.AuthorResponse;
import com.example.vann_krya_20_spring_homework003.service.AuthorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }


    //get all authors
    @GetMapping
    public ResponseEntity<?> getAllAuthor() {
        List<Author> author = authorService.getAllAuthor();
        AuthorResponse<List<Author>> response = AuthorResponse.<List<Author>>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully fetched authors")
                .payload(author)
                .build();
        return ResponseEntity.ok(response);
    }


    //get author by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getAuthorById(@PathVariable int id) {
        Author author = authorService.getAuthorById(id);
        AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully fetched author")
                .payload(author)
                .build();
        return ResponseEntity.ok(response);
    }


    //post author
    @PostMapping
    public ResponseEntity<?> postAuthor(@RequestBody AuthorRequest authorRequest) {
       Author author = authorService.postAuthor(authorRequest);
       AuthorResponse<Author> response = AuthorResponse.<Author>builder()
               .timestamp(LocalDateTime.now())
               .status(200)
               .message("Successfully added author")
               .payload(author)
               .build();
       return ResponseEntity.ok(response);
    }


    //delete author by id
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAuthorById(@PathVariable int id) {
        authorService.deleteAuthorById(id);
       AuthorResponse<Author> response = AuthorResponse.<Author>builder()
               .timestamp(LocalDateTime.now())
               .status(200)
               .message("Successfully deleted author")
               .payload(null)
               .build();
       return ResponseEntity.ok(response);
    }


    //put author by id
    @PutMapping("/{id}")
    public ResponseEntity<?> putAuthorById(@PathVariable int id, @RequestBody AuthorRequest authorRequest) {
        authorService.putAuthorById(id, authorRequest);
       AuthorResponse<Author> response = AuthorResponse.<Author>builder()
               .timestamp(LocalDateTime.now())
               .status(200)
               .message("Successfully updated author")
               .payload(authorService.getAuthorById(id))
               .build();
       return ResponseEntity.ok(response);
    }
}
