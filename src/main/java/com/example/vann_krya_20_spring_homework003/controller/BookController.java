package com.example.vann_krya_20_spring_homework003.controller;


import com.example.vann_krya_20_spring_homework003.model.entity.Book;
import com.example.vann_krya_20_spring_homework003.model.request.BookRequest;
import com.example.vann_krya_20_spring_homework003.model.response.BookResponse;
import com.example.vann_krya_20_spring_homework003.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
    private final BookService bookService;
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }


    //get all books
    @GetMapping
    public ResponseEntity<?> getAllBook() {
        List<Book> book = bookService.getAllBook();
        BookResponse<List<Book>> response = BookResponse.<List<Book>>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully fetched books")
                .payload(book)
                .build();
        return ResponseEntity.ok(response);
    }


    //get book by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getBookById(@PathVariable int id) {
        Book book = bookService.getBookById(id);
        BookResponse<Book> response = BookResponse.<Book>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully fetched book")
                .payload(book)
                .build();
        return ResponseEntity.ok(response);
    }


    //post book
    @PostMapping
    public ResponseEntity<?> postBook(@RequestBody BookRequest bookRequest) {
        int BookId = bookService.postBook(bookRequest);
        BookResponse<Book> response = BookResponse.<Book>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully added Book")
                .payload(bookService.getBookById(BookId))
                .build();
        return ResponseEntity.ok(response);
    }


    //delete book by id
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBookById(@PathVariable int id) {
        bookService.deleteBookById(id);
        BookResponse<Book> response = BookResponse.<Book>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully deleted book")
                .payload(null)
                .build();
        return ResponseEntity.ok(response);
    }


    //put book by id
    @PutMapping("/{id}")
    public ResponseEntity<?> putBookById(@PathVariable int id, @RequestBody BookRequest bookRequest) {
        bookService.putBookById(id, bookRequest);
        BookResponse<Book> response = BookResponse.<Book>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully updated Book")
                .payload(bookService.getBookById(id))
                .build();
        return ResponseEntity.ok(response);
    }


}
