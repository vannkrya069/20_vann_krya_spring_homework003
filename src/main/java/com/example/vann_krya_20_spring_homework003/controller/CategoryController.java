package com.example.vann_krya_20_spring_homework003.controller;

import com.example.vann_krya_20_spring_homework003.model.entity.Category;
import com.example.vann_krya_20_spring_homework003.model.request.CategoryRequest;
import com.example.vann_krya_20_spring_homework003.model.response.CategoryResponse;
import com.example.vann_krya_20_spring_homework003.service.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;


@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {
    private final CategoryService categoryService;
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    //get all categories
    @GetMapping
    public ResponseEntity<?> getAllCategory() {
        List<Category> category = categoryService.getAllCategory();
        CategoryResponse<List<Category>> response = CategoryResponse.<List<Category>>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully fetched categories")
                .payload(category)
                .build();
        return ResponseEntity.ok(response);
    }


    //get category by id
    @GetMapping("/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable int id) {
        Category category = categoryService.getCategoryById(id);
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully fetched category")
                .payload(category)
                .build();
        return ResponseEntity.ok(response);
    }


    //post category
    @PostMapping
    public ResponseEntity<?> postCategory(@RequestBody CategoryRequest categoryRequest) {
        Category category = categoryService.postCategory(categoryRequest);
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully added categories")
                .payload(category)
                .build();
        return ResponseEntity.ok(response);
    }


    //delete category by id
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategoryById(@PathVariable int id) {
        categoryService.deleteCategoryById(id);
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully deleted category")
                .payload(null)
                .build();
        return ResponseEntity.ok(response);
    }


    //put category by id
    @PutMapping("/{id}")
    public ResponseEntity<?> putCategoryById(@PathVariable int id, @RequestBody CategoryRequest categoryRequest) {
        categoryService.putCategoryById(id, categoryRequest);
        CategoryResponse<Category> response = CategoryResponse.<Category>builder()
                .timestamp(LocalDateTime.now())
                .status(200)
                .message("Successfully updated category")
                .payload(categoryService.getCategoryById(id))
                .build();
        return ResponseEntity.ok(response);
    }
}
