package com.example.vann_krya_20_spring_homework003.exception;

import com.example.vann_krya_20_spring_homework003.exception.type.AuthorNotFound;
import com.example.vann_krya_20_spring_homework003.exception.type.BlankField;
import com.example.vann_krya_20_spring_homework003.exception.type.BookNotFound;
import com.example.vann_krya_20_spring_homework003.exception.type.CategoryNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;
import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler {

    //not found exception for Book
    @ExceptionHandler(BookNotFound.class)
    ProblemDetail notFoundExceptionHandler(BookNotFound e) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("localhost:8080/errors/not-found"));
        problemDetail.setTitle("Book not found");
        problemDetail.setProperty("timestamp", LocalDateTime.now());
        return problemDetail;
    }


    //not found exception for Author
    @ExceptionHandler(AuthorNotFound.class)
    ProblemDetail notFoundExceptionHandler(AuthorNotFound e) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("localhost:8080/errors/not-found"));
        problemDetail.setTitle("Author not found");
        problemDetail.setProperty("timestamp", LocalDateTime.now());
        return problemDetail;
    }


    //not found exception for Category
    @ExceptionHandler(CategoryNotFound.class)
    ProblemDetail notFoundExceptionHandler(CategoryNotFound e) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setType(URI.create("localhost:8080/errors/not-found"));
        problemDetail.setTitle("Category not found");
        problemDetail.setProperty("timestamp", LocalDateTime.now());
        return problemDetail;
    }


    //blank field exception
    @ExceptionHandler(BlankField.class)
    ProblemDetail blankFieldExceptionHandler(BlankField e) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setType(URI.create("localhost:8080/errors/bad-request"));
        problemDetail.setTitle("Insert field is null");
        problemDetail.setProperty("timestamp", LocalDateTime.now());
        return problemDetail;
    }

}
