package com.example.vann_krya_20_spring_homework003.exception.type;

public class AuthorNotFound extends RuntimeException {
    public AuthorNotFound(String message) {
        super(message);
    }
}
