package com.example.vann_krya_20_spring_homework003.exception.type;

public class BlankField extends RuntimeException {
    public BlankField(String message) {
        super(message);
    }
}
