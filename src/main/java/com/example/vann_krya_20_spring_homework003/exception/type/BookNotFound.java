package com.example.vann_krya_20_spring_homework003.exception.type;

public class BookNotFound extends RuntimeException{
    public BookNotFound(String message) {
        super(message);
    }
}
