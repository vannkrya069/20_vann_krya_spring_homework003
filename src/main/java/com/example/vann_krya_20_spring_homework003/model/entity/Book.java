package com.example.vann_krya_20_spring_homework003.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class Book {
    private int bookId;
    private String title;
    private LocalDateTime publishedDate;
    private Author author;
    private List<Category> category;
}
