package com.example.vann_krya_20_spring_homework003.model.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Category {
    private int categoryId;
    private String categoryName;
}
