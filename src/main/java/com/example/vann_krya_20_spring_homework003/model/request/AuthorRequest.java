package com.example.vann_krya_20_spring_homework003.model.request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class AuthorRequest {
    private String authorName;
    private String gender;
}
