package com.example.vann_krya_20_spring_homework003.model.request;

import com.example.vann_krya_20_spring_homework003.model.entity.Author;
import com.example.vann_krya_20_spring_homework003.model.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class BookRequest {
    private String title;
    private LocalDateTime publishedDate;
    private int authorId;
    private List<Integer> categoryId;
}
