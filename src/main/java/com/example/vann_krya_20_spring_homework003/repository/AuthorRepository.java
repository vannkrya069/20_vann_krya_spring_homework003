package com.example.vann_krya_20_spring_homework003.repository;


import com.example.vann_krya_20_spring_homework003.model.entity.Author;
import com.example.vann_krya_20_spring_homework003.model.request.AuthorRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepository {

    //get all authors
    @Select("""
            select * from authors
            """)
    @Results(id = "authorMapper", value = {
            @Result(property = "authorId", column = "author_id"),
            @Result(property = "authorName", column = "author_name")
    })
    List<Author> getAllAuthor();


    //get author by id
    @Select("""
            select * from authors
            where author_id = #{id}
            """)
    @ResultMap("authorMapper")
    Author getAuthorById(int id);


    //post author
    @Select("""
            insert into authors(author_name, gender)
            values(#{author.authorName}, #{author.gender})
            returning *
            """)
    @ResultMap("authorMapper")
    Author postAuthor(@Param("author") AuthorRequest authorRequest);


    //delete author by id
    @Delete("""
            delete from authors
            where author_id = #{id}
            """)
    @ResultMap("authorMapper")
    void deleteAuthorById(int id);


    //put author by id
    @Select("""
            update authors
            set author_name = #{author.authorName}, gender = #{author.gender}
            where author_id = #{id}
            """)
    @ResultMap("authorMapper")
    Author putAuthorById(int id, @Param("author") AuthorRequest authorRequest);
}
