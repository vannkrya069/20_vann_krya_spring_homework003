package com.example.vann_krya_20_spring_homework003.repository;

import com.example.vann_krya_20_spring_homework003.model.entity.Book;
import com.example.vann_krya_20_spring_homework003.model.request.BookRequest;
import org.apache.ibatis.annotations.*;


import java.util.List;

@Mapper
public interface BookRepository {

    //get all books
    @Select("""
            select * from books
            """)
    @Results(id = "bookMapper" , value = {
            @Result(property = "bookId", column = "book_id"),
            @Result(property = "publishedDate", column = "published_date"),
            @Result(property = "author", column = "author_id",
                    one = @One(select = "com.example.vann_krya_20_spring_homework003.repository.AuthorRepository.getAuthorById")
            ),
            @Result(property = "category", column = "book_id",
                    many = @Many(select = "com.example.vann_krya_20_spring_homework003.repository.CategoryRepository.getAllBookById")
            )
    })
    List<Book> getAllBook();


    //get book by id
    @Select("""
            select * from books
            where book_id = #{id}
            """)
    @ResultMap("bookMapper")
    Book getBookById(int id);


    //post book
    @Select("""
            insert into books(title, published_date, author_id)
            values (#{books.title}, #{books.publishedDate}, #{books.authorId})
            returning book_id
            """)
    int books(@Param("books") BookRequest bookRequest);
    @Select("""
            insert into book_details(book_id, category_id)
            values(#{bookId}, #{categoryId})
            """)
    Integer bookDetails(int bookId, int categoryId);


    //delete book by id
    @Delete("""
            delete from books
            where book_id = #{id}
            """)
    void deleteBookById(int id);


    //put book by id
    @Select("""
            update books
            set author_id = #{books.authorId}, title = #{books.title}, published_date = #{books.publishedDate}
            where book_id = #{id}
            returning book_id
            """)
    Integer putBookById(int id, @Param("books") BookRequest bookRequest);
    @Delete("""
            delete from book_details
            where book_id = #{id}
            """)
    void deleteBookIdFromBookDetailTable(int id);

}
