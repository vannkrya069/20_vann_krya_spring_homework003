package com.example.vann_krya_20_spring_homework003.repository;


import com.example.vann_krya_20_spring_homework003.model.entity.Category;
import com.example.vann_krya_20_spring_homework003.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {

    //get all categories
    @Select("""
            select * from categories
            """)
    @Results(id = "categoryMapper", value = {
            @Result(property = "categoryId", column = "category_id"),
            @Result(property = "categoryName", column = "category_name")
    })
    List<Category> getAllCategory();


    //get category by id
    @Select("""
            select * from categories
            where category_id = #{id}
            """)
    @ResultMap("categoryMapper")
    Category getCategoryById(int id);


    //post category
    @Select("""
            insert into categories(category_name)
            values(#{categoryName})
            returning *
            """)
    @ResultMap("categoryMapper")
    Category postCategory(CategoryRequest categoryRequest);


    //delete category by id
    @Delete("""
            delete from categories
            where category_id = #{id}
            """)
    @ResultMap("categoryMapper")
    void deleteCategoryById(int id);


    //put category by id
    @Select("""
            update categories
            set category_name = #{category.categoryName}
            where category_id = #{id}
            """)
    @ResultMap("categoryMapper")
    Category putCategoryById(int id, @Param("category") CategoryRequest categoryRequest);


    //get all books by id from BookRepository many to many
    @Select("""
            select * from categories
            inner join book_details on categories.category_id = book_details.category_id
            where book_id = #{id}
            """)
    @ResultMap("categoryMapper")
    List<Category> getAllBookById(int id);
}
