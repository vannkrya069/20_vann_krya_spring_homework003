package com.example.vann_krya_20_spring_homework003.service;

import com.example.vann_krya_20_spring_homework003.model.entity.Author;
import com.example.vann_krya_20_spring_homework003.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();
    Author getAuthorById(int id);
    Author postAuthor(AuthorRequest authorRequest);
    void deleteAuthorById(int id);
    Author putAuthorById(int id, AuthorRequest authorRequest);
}
