package com.example.vann_krya_20_spring_homework003.service;

import com.example.vann_krya_20_spring_homework003.model.entity.Book;
import com.example.vann_krya_20_spring_homework003.model.request.BookRequest;

import java.util.List;

public interface BookService {
    List<Book> getAllBook();
    Book getBookById(int id);
    int postBook(BookRequest bookRequest);
    void deleteBookById(int id);
    Integer putBookById(int id, BookRequest bookRequest);
}
