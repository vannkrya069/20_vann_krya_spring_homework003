package com.example.vann_krya_20_spring_homework003.service;

import com.example.vann_krya_20_spring_homework003.model.entity.Category;
import com.example.vann_krya_20_spring_homework003.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategory();
    Category getCategoryById(int id);
    Category postCategory(CategoryRequest categoryRequest);
    void deleteCategoryById(int id);
    Category putCategoryById(int id, CategoryRequest categoryRequest);
}
