package com.example.vann_krya_20_spring_homework003.service.implement;

import com.example.vann_krya_20_spring_homework003.exception.type.AuthorNotFound;
import com.example.vann_krya_20_spring_homework003.exception.type.BlankField;
import com.example.vann_krya_20_spring_homework003.model.entity.Author;
import com.example.vann_krya_20_spring_homework003.model.request.AuthorRequest;
import com.example.vann_krya_20_spring_homework003.repository.AuthorRepository;
import com.example.vann_krya_20_spring_homework003.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorServiceImp implements AuthorService {

    private AuthorRepository authorRepository;
    @Autowired
    public void setAuthorRepository(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }


    @Override
    public List<Author> getAllAuthor() {
        if(authorRepository.getAllAuthor().isEmpty()) {
            throw new AuthorNotFound("No authors in table list");
        }
        return authorRepository.getAllAuthor();
    }


    @Override
    public Author getAuthorById(int id) {
        Author author = authorRepository.getAuthorById(id);
        if(author == null) {
            throw new AuthorNotFound("Author with id : " + id + " not found");
        }
        return author;
    }


    @Override
    public Author postAuthor(AuthorRequest authorRequest) {
        Author author = authorRepository.postAuthor(authorRequest);
        if(authorRequest.getAuthorName().isBlank()) {
            throw new BlankField("Field authorName is Empty");
        }
        if(authorRequest.getGender().isBlank()) {
            throw new BlankField("Field gender is Empty");
        }
        return author;
    }


    @Override
    public void deleteAuthorById(int id) {
        if(authorRepository.getAuthorById(id) == null) {
            throw new AuthorNotFound("Author with id : " + id + " not found");
        }else {
            authorRepository.deleteAuthorById(id);
        }
    }


    @Override
    public Author putAuthorById(int id, AuthorRequest authorRequest) {
        if(authorRepository.getAuthorById(id) == null) {
            throw new AuthorNotFound("Author with id : " + id + " not found");
        }else {
            Author author = authorRepository.putAuthorById(id, authorRequest);
            if(authorRequest.getAuthorName().isBlank()) {
                throw new BlankField("Field authorName is Empty");
            }
            if(authorRequest.getGender().isBlank()) {
                throw new BlankField("Field gender is Empty");
            }
            return author;
        }
    }
}
