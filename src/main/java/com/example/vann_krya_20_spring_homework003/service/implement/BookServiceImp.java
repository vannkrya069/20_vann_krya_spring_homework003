package com.example.vann_krya_20_spring_homework003.service.implement;

import com.example.vann_krya_20_spring_homework003.exception.type.BlankField;
import com.example.vann_krya_20_spring_homework003.exception.type.BookNotFound;
import com.example.vann_krya_20_spring_homework003.model.entity.Book;
import com.example.vann_krya_20_spring_homework003.model.request.BookRequest;
import com.example.vann_krya_20_spring_homework003.repository.BookRepository;
import com.example.vann_krya_20_spring_homework003.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    private BookRepository bookRepository;
    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    @Override
    public List<Book> getAllBook() {
        List<Book> book = bookRepository.getAllBook();
        if(book.isEmpty()) {
            throw new BookNotFound("No books in table list");
        }
        return book;
    }


    @Override
    public Book getBookById(int id) {
        Book book = bookRepository.getBookById(id);
        if(book == null) {
            throw new BookNotFound("Book with id : " + id + " not found");
        }
        return book;
    }


    @Override
    public int postBook( BookRequest bookRequest) {
        if(bookRequest.getTitle().isBlank()) {
            throw new BlankField("Field title is Empty");
        }
        int BookId = bookRepository.books(bookRequest);
        for (int CategoryId : bookRequest.getCategoryId()
             ) {
            bookRepository.bookDetails(BookId, CategoryId);
        }
        return BookId;
    }


    @Override
    public void deleteBookById(int id) {
        if(bookRepository.getBookById(id) == null) {
            throw new BookNotFound("Book with id : " + id + " not found");
        } else {
            bookRepository.deleteBookById(id);
        }
    }


    @Override
    public Integer putBookById(int id, BookRequest bookRequest) {
        if(bookRepository.getBookById(id) == null) {
            throw new BookNotFound("Book with id : " + id + " not found");
        } else {
            Integer updateId = bookRepository.putBookById(id, bookRequest);
           if(bookRequest.getTitle().isBlank()) {
               throw new BlankField("Field title is Empty");
           }
            if(updateId != null) {
                bookRepository.deleteBookIdFromBookDetailTable(updateId);
                for (int Id : bookRequest.getCategoryId()
                ) {
                    bookRepository.bookDetails(updateId, Id);
                }
                return bookRepository.putBookById(id, bookRequest);
            }
            return updateId;
        }
    }


}
