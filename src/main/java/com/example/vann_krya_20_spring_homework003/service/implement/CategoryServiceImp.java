package com.example.vann_krya_20_spring_homework003.service.implement;

import com.example.vann_krya_20_spring_homework003.exception.type.BlankField;
import com.example.vann_krya_20_spring_homework003.exception.type.CategoryNotFound;
import com.example.vann_krya_20_spring_homework003.model.entity.Category;
import com.example.vann_krya_20_spring_homework003.model.request.CategoryRequest;
import com.example.vann_krya_20_spring_homework003.repository.CategoryRepository;
import com.example.vann_krya_20_spring_homework003.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CategoryServiceImp implements CategoryService {

    private CategoryRepository categoryRepository;
    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<Category> getAllCategory() {
        List<Category> category = categoryRepository.getAllCategory();
        if(category.isEmpty()) {
            throw new CategoryNotFound("No Category in table list");
        }
        return category;
    }


    @Override
    public Category getCategoryById(int id) {
        Category category = categoryRepository.getCategoryById(id);
        if(category == null) {
            throw new CategoryNotFound("Category with id : " + id + " not found");
        }
        return category;
    }


    @Override
    public Category postCategory(CategoryRequest categoryRequest) {
        Category category = categoryRepository.postCategory(categoryRequest);
        if(categoryRequest.getCategoryName().isBlank()) {
            throw new BlankField("Field categoryName is Empty");
        }
        return category;
    }


    @Override
    public void deleteCategoryById(int id) {
        if(categoryRepository.getCategoryById(id) == null) {
            throw new CategoryNotFound("Category with id : " + id + " not found");
        }else {
            categoryRepository.deleteCategoryById(id);
        }
    }


    @Override
    public Category putCategoryById(int id, CategoryRequest categoryRequest) {
        if(categoryRepository.getCategoryById(id) == null) {
            throw new CategoryNotFound("Category with id : " + id + " not found");
        }else {
            Category category = categoryRepository.putCategoryById(id, categoryRequest);
            if(categoryRequest.getCategoryName().isBlank()) {
                throw new BlankField("Field categoryName is Empty");
            }
            return category;
        }

    }
}
