create table authors(
                    author_id serial primary key ,
                    author_name varchar(50) not null ,
                    gender varchar(50) not null
);

create table books(
                    book_id serial primary key ,
                    title varchar(255) not null ,
                    published_date timestamp,
                    author_id int references authors on delete cascade on update cascade
);

create table categories(
                    category_id serial primary key ,
                    category_name varchar(255) not null
);

create table book_details(
                    id serial primary key ,
                    book_id int references books on delete cascade on update cascade,
                    category_id int references categories on delete cascade on update cascade
);